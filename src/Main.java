import auth.AuthenticationInformations;
import auth.ClassicAuthentication;
import auth.Role;
import auth.User;

public class Main {
    public static void main(String[] args) {

        /*System.out.println("Hello world!");
        Enfant enfant = new Enfant("Agobert", "Quentin");
        System.out.println(enfant.getNom());*/

        User.createUser("Quentin");
        User.createUser("Agobert", "Quentin", "Quintin", "q@a.com", "kaamelott");
        User.displayUsers(User.usersWithSpecificRole(Role.ADMIN));

        AuthenticationInformations authInfos = new AuthenticationInformations("Nciolas");
        ClassicAuthentication classicAuth = new ClassicAuthentication();
        System.out.println(classicAuth.connection(authInfos) ? "Connexion" : "Echec");
    }
}