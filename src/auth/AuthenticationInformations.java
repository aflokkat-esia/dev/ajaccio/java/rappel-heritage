package auth;

public class AuthenticationInformations {
    private String username;

    private String email;
    private String password;

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public AuthenticationInformations(String username) {
        this.username = username;
    }

    public AuthenticationInformations(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
