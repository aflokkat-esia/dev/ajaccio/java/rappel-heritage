package auth;

public interface AuthenticationSystem {
    public boolean connection(AuthenticationInformations authInfos);
}
