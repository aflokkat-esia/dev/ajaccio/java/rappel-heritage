package auth;

import java.util.ArrayList;
import java.util.Objects;

public class User {
    private String lastname, firstname, username, email, password;
    private Role role;

    static ArrayList<User> users = new ArrayList<>();

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public static void displayUsers() {
        if (users.size() == 0) {
            System.out.println("Pas d'utilisateur...");
        } else {
            System.out.println("Liste des utilisateurs : ");
            for (User user : users) {
                System.out.println(" - " + user.username + ", " + user.role);
            }
        }
    }

    public static void displayUsers(ArrayList<User> users) {
        if (users.size() == 0) {
            System.out.println("Pas d'utilisateur...");
        } else {
            System.out.println("Liste des utilisateurs : ");
            for (User user : users) {
                System.out.println(" - " + user.username + ", " + user.role);
            }
        }
    }

    public static User createUser(String username) {
        for (User user : users) {
            if (Objects.equals(user.username, username)) {
                System.out.println("Ce pseudo existe déjà...");
                return null;
            }
        }

        return new User(username);
    }

    public static User createUser(String lastname, String firstname, String username, String email, String password) {
        for (User user : users) {
            if (Objects.equals(user.username, username)) {
                System.out.println("Ce pseudo existe déjà...");
                return null;
            }
        }

        return new User(lastname, firstname, username, email, password);
    }

    public static ArrayList<User> usersWithSpecificRole(Role role) {
        ArrayList<User> tmp = new ArrayList<>();
        for (User user : users) {
            if (user.role == role) {
                tmp.add(user);
            }
        }

        return tmp;
    }

    private User(String username) {
        this.username = username;
        this.role = Role.VISITEUR;

        users.add(this);
    }

    private User(String lastname, String firstname, String username, String email, String password) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = Role.UTILISATEUR;

        users.add(this);
    }

    public void infos() {
        if (this.role == Role.VISITEUR) {
            System.out.println(this.username);
        } else {
            System.out.println(this.firstname + " " + this.lastname);
        }
    }
}
