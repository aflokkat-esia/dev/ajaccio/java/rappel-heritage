package auth;

public enum Role {
    VISITEUR,
    UTILISATEUR,
    ADMIN,
    SUPERADMIN
}
