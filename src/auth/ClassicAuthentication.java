package auth;

import java.util.Objects;

public class ClassicAuthentication implements AuthenticationSystem {

    @Override
    public boolean connection(AuthenticationInformations authInfos) {
        if (authInfos.getUsername() != null) {
            for (User user : User.users) {
                if (Objects.equals(user.getUsername(), authInfos.getUsername())) {
                    return true;
                }
            }
        } else {
            for (User user : User.users) {
                if (Objects.equals(user.getEmail(), authInfos.getEmail()) && Objects.equals(user.getPassword(), authInfos.getPassword())) {
                    return true;
                }
            }
        }
        return false;
    }
}
