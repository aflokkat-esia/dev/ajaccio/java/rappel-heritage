public class Enfant extends Parent implements Interface{

    private String surnom;

    public Enfant(String nom, String surnom) {
        super(nom); // Constructeur du parent
        this.surnom = surnom;
        System.out.println(super.getNom());
    }

    @Override
    public String getNom() {
        return super.getNom() + ", " + this.surnom;
    }


    @Override
    public void affichage() {
        System.out.println(Interface.toto);
    }
}
