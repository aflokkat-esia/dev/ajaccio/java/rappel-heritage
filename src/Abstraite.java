public abstract class Abstraite {
    protected String toto = "Toto";

    public String getToto() {
        return toto;
    }

    public abstract void setToto(String toto);
}
